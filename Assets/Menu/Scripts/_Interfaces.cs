using System.Collections.Generic;

namespace JMD.Menu
{

  /* interficies para componentes de Unity */
  public interface IGrController
  {
    // genera los botones asignando una acción a cada uno de ellos
    void InitMenu();
    // permite a los botones ejecutar las acciones que tienen asignadas
    void ExecuteAction(string action);
  }

  interface IGrMenuContainer
  {
    void GenerateElements(IGrController controller, IMenu menu);

  }

  interface IGrButtonContainer
  {
    void GenerateButton(IGrController controller, IMenuElement button);
      
  }

  /* interficies para entidades */
  public interface IMenu
  {
    string target { get; set; }
    string title { get; set; }
    List<IMenuElement> items { get; set; }

  }
  public interface IMenuElement
  {
    // texto del botón
    string text { get; set; }
    // nombre del recurso del botón que se importará
    string resource { get; set; }
    // accion a realizar por el botón
    IMenuAction action { get; set; }
  }

  public interface IMenuAction
  {
    eActionType type{get; set;}
    string name{get; set;}
    List<string> parameters{get; set;}
      
  }


  // enumerados
  public enum eActionType
  {
     LOAD_PG
  }

}