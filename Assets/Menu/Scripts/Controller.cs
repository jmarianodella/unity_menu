﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JMD.Menu
{

  public class Controller : MonoBehaviour, IGrController
  {
    const string CONTAINER = "MenuContainerButtons";
    // GameObject container;
    public void ExecuteAction(string action)
    {
      Debug.Log("action " + action);
    }

    public void InitMenu()
    {
      // load menu from json inside Level
      lMenu = mockGetData();
      // Para cada menú definido en la lista
      // pasamos la lista de elementos al contenedor

      GameObject container;
      // buscamos el contenedor del menu
      container = GameObject.Find(lMenu[0].target);
      container.GetComponent<IGrMenuContainer>().GenerateElements(this, lMenu[0]);

    }
    private List<IMenu> lMenu;
    void Awake()
    {
    }
    // Use this for initialization
    void Start()
    {
      // checkContainer();
      Debug.Log("iniciando menu");
      InitMenu();

    }

    // Update is called once per frame
    void Update()
    {

    }

    // genera los contenedores de los botones
    private void generateButtonContainer()
    {

    }

    // falsea datos externos
    List<IMenu> mockGetData()
    {
      List<IMenu> lMenu = new List<IMenu>();

      IMenu menu = new Menu("Titol menu 1", "Menu1");
      string resourceButtonName = "BasicButton";
      for (int i = 0; i < 10; i++)
      {
        //generamos falso array
        IMenuAction action = new MenuAction();
        action.name = "Action_" + i;
        action.type = eActionType.LOAD_PG;
        string text = "Text_" + i;
        IMenuElement element = new MenuElement(text, resourceButtonName, action);

        menu.items.Add(element);
      }

      lMenu.Add(menu);
      return lMenu;
    }

  }

}