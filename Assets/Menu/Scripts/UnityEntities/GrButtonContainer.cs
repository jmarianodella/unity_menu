﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JMD.Menu.Button;

namespace JMD.Menu
{

  public class GrButtonContainer : MonoBehaviour, IGrButtonContainer
  {
		GameObject buttonGameObject;
		JMD.Menu.Button.IController buttonController;
    public void GenerateButton(IGrController controller, IMenuElement button)
    {
			buttonGameObject = recoverResource(button);
			buttonController = buttonGameObject.GetComponent<IController>();
			buttonController.button = button;
			buttonController.controller = controller;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private GameObject recoverResource(IMenuElement menuItem)
    {
      GameObject button;
      GameObject obj = Resources.Load<GameObject>(menuItem.resource);
      button = Instantiate(obj);
      button.transform.SetParent(transform);
      return button;
    }
  }
}
