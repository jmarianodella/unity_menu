﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JMD.Menu
{

  public class GrMenuContainer : MonoBehaviour, IGrMenuContainer
  {
    // elementos botones
    // List<GameObject> button = new List<GameObject>();
    public void GenerateElements(IGrController controller, IMenu menu)
    {
      // para cada elemento del menú generamos un botón y lo hacemos hijo
      foreach (IMenuElement item in menu.items)
      {
        GameObject bMenu = recoverResource("ButtonContainer");
				IGrButtonContainer bContainer = bMenu.GetComponent<IGrButtonContainer>();
				bContainer.GenerateButton(controller, item);
      }

    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private GameObject recoverResource(string buttonContainer)
    {
      GameObject button;
      GameObject obj = Resources.Load<GameObject>(buttonContainer);
      button = Instantiate(obj);
      button.transform.SetParent(transform);
      return button;
    }
  }

}