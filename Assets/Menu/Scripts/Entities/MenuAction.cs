

using System.Collections.Generic;

namespace JMD.Menu
{
  public class MenuAction : IMenuAction
  {
    public eActionType type
    {
      get { return _type; }
      set { _type = value; }
    }

    public string name
    {
      get { return _name; }
      set { _name = value; }
    }
    public List<string> parameters
    {
      get { return _parameters; }
      set { _parameters = value; }
    }

    // variables privadas
    eActionType _type;
    string _name;
    List<string> _parameters;

  }

}