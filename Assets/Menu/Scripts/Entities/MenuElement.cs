
using System.Collections.Generic;

namespace JMD.Menu
{
  public class MenuElement : IMenuElement
  {
    public IMenuAction action
    {
      get { return _action; }
      set { _action = value; }
    }

    public string resource
    {
      get { return _resource; }
      set { _resource = value; }
    }
    public string text
    {
      get { return _text; }
      set { _text = value; }
    }

    public MenuElement(string text, string resource, IMenuAction action){
      this.action = action;
      this.resource = resource;
      this.text = text;
    }
    // public void copy(IMenuElement menu)
    // {
    //   this.action = menu.action;
    //   this.resource = menu.resource;
    //   this.text = menu.text;
    // }
    // private fields

    string _text;
    string _resource;
    IMenuAction _action;
  }

}