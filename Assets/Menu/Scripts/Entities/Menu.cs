
using System.Collections.Generic;

namespace JMD.Menu
{
  public class Menu : IMenu
  {
    public string target
    {
     
      get { return _target; }
      set { _target = value; }
    }

    public string title
    {
      get { return _title; }
      set { _title = value; }
    }
    public List<IMenuElement> items
    {
      get { return _items; }
      set { _items = value; }
    }

    public Menu (string title, string target){
      this.title = title;
      this.target = target;
      this.items = new List<IMenuElement>();
    }
    // private fields
    string _target;
    string _title;
    List<IMenuElement> _items;
  }

}