﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using JMD.Menu;

namespace JMD.Menu.Button

{
  public class Controller : MonoBehaviour, JMD.Menu.Button.IController
  {

    public string textButton = "";


    public JMD.Menu.IMenuElement button
    {
      get { return _button; }

      set
      {
        _button = value;
        updateTextButton(_button.text);
      }
    }
    // public string action
    // {
    //   get { return _action; }
    //   set { _action = value; }
    // }

    public JMD.Menu.IGrController controller
    {
      get { return _menuController; }
      set { _menuController = value; }
    }

    private JMD.Menu.IMenuElement _button;
    private string _action;
    private JMD.Menu.IGrController _menuController;
    // Use this for initialization
    void Start()
    {
      if (textButton.Length > 0)
      {
        updateTextButton(textButton);
      }
      RectTransform rt = gameObject.GetComponent<RectTransform>();
      // rt.rect.Set(0, 0, 0, 0);
      Debug.LogWarning(rt.rect.yMax);
      rt.offsetMax = Vector2.zero;
      rt.offsetMin = Vector2.zero;

    }


    private void updateTextButton(string txt)
    {
      GameObject textContainer = gameObject.transform.Find("Text").gameObject;
      textContainer.GetComponent<TMP_Text>().text = txt;
    }



    public void onClick()
    {
      _menuController.ExecuteAction(button.action.name);
    }
  }

}