using JMD.Menu;

namespace JMD.Menu.Button
{


  public interface IController
  {
    // información del botón
    JMD.Menu.IMenuElement button {get; set;}
    // controlador del menú
    JMD.Menu.IGrController controller { get; set; }
    // acción a realizar cuando se pica sobre el botón
    void onClick();

  }


}